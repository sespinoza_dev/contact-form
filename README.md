# Contact Form

Microservice that manage a form contact. It exposes a REST API and sends a contact form information to my email when someone completes the form in my personal web page. It uses `sendgrid`. Feel free to customize it to fit your own necessities.

Note: you will need to create a [sengrid account](https://sendgrid.com/) and verify a [sender identity](https://app.sendgrid.com/settings/sender_auth).

Note2: You can always start by checking the [sengrid nodejs documentation](https://github.com/sendgrid/sendgrid-nodejs)

## Installation

```bash
npm install
```

Please, set the following environmental variables. 
You can achieve this by placing them in a file and then exporting them.

```
# contact form service
export CONTACT_BASE_URL="/contact"

# sengrid
export RECEIVER=test@test.com
export SENDGRID_VERIFIED_SENDER=test@test.com
export SENDGRID_API_KEY=MY-KEY
```

## Usage

Start the application with:

```bash
npm start
```

## API

| method | endpoint | response                                                                         |
| ------ | -------- | -------------------------------------------------------------------------------- |
| GET    | `/api`   | `{  "service": "contact-form",  "version": "1.0.0" }`                            |
| GET    | `/send`  | `{ "name": "Jon", "sender": "snow@winter.com", "message": "Winter is comming" }` |

## Try out

Example of request using `curl`:

```bash
curl -s -XPOST http://localhost:4001/contact/send -H 'Content-Type: application/json' -d '{ "name": "Jon", "sender": "snow@winter.com", "message": "Winter is comming" }'
```

## Deploy

One way to deploy it is using docker. For that create an image and then, if you want, push it to your docker registry.

```bash
docker build -t my-docker-account/my-service-name:latest .
docker push my-docker-account/my-service-name
```

After that you can pull the image and use docker-compose to manage the containers. A configuration for a docker-compose.yml can be the following. Change it as needed.

```yaml
version: '2'

services:
  rut:
    container_name: contact-form
    image: my-docker-account/my-service-name:latest
    restart: always
    ports:
      - 4000:4000
    environment:
      - CONTACT_BASE_URL=/
      - RECEIVER=test@test.cl
      - SENDGRID_VERIFIED_SENDER="test@test.cl"
      - SENDGRID_API_KEY=THIS_IS_THE_KEY
```

With that you can pull the image and run it in the background with:

```bash
docker-compose pull
docker-compose up -d
```

To stop the service with

```bash
docker-compose down
```

## Contributing

1. Clone the project (`git clone git@github.com:sespinoza-dev/contact-form.git && cd contact-form`)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am "Add some feature"`)
4. Push to the branch (`git push --set-upstream origin my-new-feature`)
5. Create a new Merge Request/Pull request.

## Contributors

- [[sespinoz]](https://github.com/sespinoza-dev) Samuel Espinoza - creator, maintainer
