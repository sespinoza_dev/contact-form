require('dotenv').config({ path: './data/.env' })
const logger = require('./src/logger')
const { name, version } = require('./package.json')
const { startExpress } = require('./src/server')

if (!process.env.SENDGRID_API_KEY) {
  throw Error('SENDGRID_API_KEY not set!')
}

if (!process.env.RECEIVER) {
  throw Error('RECEIVER not set')
}

if (!process.env.SENDGRID_VERIFIED_SENDER) {
  throw Error('RECEIVER not set')
}

if (!process.env.CONTACT_BASE_URL) {
  throw Error('CONTACT_BASE_URL not set')
}

logger.info(`Starting service ${name}-${version}`)
startExpress()