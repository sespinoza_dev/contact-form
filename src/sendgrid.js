const sgMail = require('@sendgrid/mail')
const logger = require('./logger')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)
const receiver = process.env.RECEIVER
const sendgridVerifiedSender = process.env.SENDGRID_VERIFIED_SENDER 

const send = async (name, sender, msg) => {
  const payload = {
    to: receiver,
    from: sendgridVerifiedSender,
    subject: `[contact form] ${name} sent you a message`,
    text: msg,
    html: `<div> from: ${name} - ${sender}</div><div>${msg}</div>`,
  }
  logger.info('sending...')
  logger.info(payload)
  await sgMail.send(payload)
  return
}

module.exports = {
  send
}
