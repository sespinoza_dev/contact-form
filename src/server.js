const express = require('express')
const cors = require('cors')
const { name, version } = require('../package.json')
const bparser = require('body-parser')
const CONTACT_BASE_URL = process.env.CONTACT_BASE_URL
const logger = require('./logger')
const sengrid = require('./sendgrid')

const app = express()
app.use(cors())
app.use(bparser.json({ type: 'application/json' }))
app.use(bparser.urlencoded({ extended: true }))

const notFound = (_, res) => {
  res.status(404).json({ reason: 'Not found', message: 'These are not the pages you are looking for!' });
}

module.exports.startExpress = () => {
  console.log(CONTACT_BASE_URL)
  app.get(`${CONTACT_BASE_URL}/api`, (_, res) => res.status(200).json({ name, version }))
  
  app.post(`${CONTACT_BASE_URL}/api/send`, async (req, res) => {
    try {
      logger.info('POST /send')
      const name = req.body.name
      const sender = req.body.sender
      const message = req.body.message
      if (!name) {
        logger.warn('bad request, missing name')
        return res.status(400).json({ message: 'bad request'})
      }
      if (!sender) {
        logger.warn('bad request, missing sender')
        return res.status(400).json({ message: 'bad request'})
      }
      if (!message) {
        logger.warn('bad request, missing message')
        return res.status(400).json({ message: 'bad request'})
      }
      logger.info(`name ${name}, email ${sender}`)
      await sengrid.send(name, sender, message)
      logger.info(`mail sent, from: ${sender}`)
      return res.status(200).json({ status: 'ok'})
    } catch (error) {
      logger.error(error)
      return res.status(500).json(error.message)
    }
  })
  
  app.all('*', notFound)

  app.listen(4000, () => logger.info('listening in on 4000'))

  return app
}
